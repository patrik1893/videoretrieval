package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class RelevanceTester {
	public static void main(String[] args) throws IOException {
		 BufferedReader gt = new BufferedReader(new FileReader("evaluation/gt003"));
		 
		 HashMap<String, ArrayList<SimpleEntry<Integer, Integer>>> hm = new HashMap<String, ArrayList<SimpleEntry<Integer, Integer>>>();
		 String zeile = "";

		 int gtdif = 0;
	    while( (zeile = gt.readLine()) != null )
	    {
	    	gtdif++;
	    	String[] parts = zeile.split(" ");
	    	String name = parts[0].replace('/', '_');
	    	
	    	String[] parts2 = parts[1].split(":");
	    	String[] parts3 = parts[2].split(":");
	    	
	    	int start = Integer.parseInt(parts2[0]) * 60 + Integer.parseInt(parts2[1]);
	    	int end = Integer.parseInt(parts3[0]) * 60 + Integer.parseInt(parts3[1]);
	    	
	    	if (hm.containsKey(name)){
	    		hm.get(name).add(new SimpleEntry<Integer, Integer>(start, end));
	    	}
	    	else{
	    		ArrayList<SimpleEntry<Integer, Integer>> al = new ArrayList<SimpleEntry<Integer, Integer>>();
	    		al.add(new SimpleEntry<Integer, Integer>(start, end));
	    		hm.put(name, al);
	    	}
	    }
		 gt.close();
		 
		 BufferedReader result = new BufferedReader(new FileReader("evaluation/300pg003"));

		 int rCounter = 0;
		 int zCounter = 0;
		 double ap= 0.0;
		 
		 LinkedList<Boolean> al = new LinkedList<Boolean>();
		 HashSet<String> difvideos = new HashSet<String>();

		 LinkedList<String> cliplist = new LinkedList<String>();
		 
	    while( (zeile = result.readLine()) != null )
	    {
	    	
	    	String[] parts = zeile.split(" ");
	    	String[] parts2 = parts[1].split("_keyframes_");
	    	
	    	String name = parts2[0];
	    	int sec = Integer.parseInt(parts2[1]);
	    	
	    	
	    	cliplist.addFirst(name);
	    	
	    	boolean found = false;
	    	if (hm.containsKey(name)){
	    		for (SimpleEntry<Integer, Integer> entry : hm.get(name)){
	    			if ((entry.getKey() <= sec) && (sec <= entry.getValue() + 6)){
	    				found = true;
	    				difvideos.add(name + entry.getKey());
	    			}
//		    		System.out.println(entry.getKey() + " " + entry.getValue() + " " + sec);
	    		}
	    	}
	    	if (found){
	    		System.out.println("RELEVANT " + name + " " + sec);
	    	}else{
	    		System.out.println("NOT " + name + " " + sec);
	    	}
	    	al.addFirst(found);
	    }
	    
	    for (Boolean b : al){
	    	zCounter++;
	    	if (b){
	    		rCounter++;
	    		ap = ap + ((double)rCounter/(double)zCounter);
	    	}
	    }
		 HashSet<String> difclips = new HashSet<String>();

		 int c = 0;
		 double map = 0.0;
	    for (int i = 0; i < cliplist.size(); i++){
	    	if (!difclips.contains(cliplist.get(i))){
	    		difclips.add(cliplist.get(i));
	    		if (hm.containsKey(cliplist.get(i))){
	    			c ++;
	    			map = map + ((double) c / (double)(difclips.size()));
//	    			System.out.println("C " + c);
//	    			System.out.println("MAP " + map);
	    		}
	    	}
	    }
	    
	    System.out.println(difvideos.size() + " different clips found");
	    System.out.println(gtdif + " different clips in GT");
	    System.out.println("Clip MAP " + (map / c));
	    System.out.println(((double) c / (double) difclips.size()) + " Clips recall");
	    System.out.println(rCounter);
	    System.out.println(ap / rCounter);
	    result.close();
	}
}
