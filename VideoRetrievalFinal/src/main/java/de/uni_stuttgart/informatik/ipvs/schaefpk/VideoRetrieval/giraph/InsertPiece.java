package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.giraph.block_app.framework.api.BlockMasterApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerReceiveApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerSendApi;
import org.apache.giraph.block_app.framework.piece.Piece;
import org.apache.giraph.block_app.framework.piece.PieceWithWorkerContext;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexReceiver;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexSender;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.edge.EdgeFactory;
import org.apache.giraph.function.ObjectTransfer;
import org.apache.giraph.function.Supplier;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.time.SystemTime;
import org.apache.giraph.types.NoMessage;
import org.apache.giraph.utils.ArrayWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.BinaryDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTFeatureList;

public class InsertPiece extends Piece<LongWritable, VertexValue, DoubleWritable, NoMessage, Object> { 
	
	public static String NEW_VERTEX_BROADCAST_KEY = "NEW_VERTEX";
	
    private final Supplier<ArrayList<Boolean>> matchSupplier;
    private final Supplier<ArrayList<Double>> thresholds;
	
	


	public InsertPiece(Supplier<ArrayList<Boolean>> matchSupplier, Supplier<ArrayList<Double>> thresholds) {
		super();
		this.matchSupplier = matchSupplier;
		this.thresholds = thresholds;
	}




	public VertexSender<LongWritable, VertexValue, DoubleWritable> getVertexSender(
	        final BlockWorkerSendApi<LongWritable, VertexValue, DoubleWritable,
	        NoMessage> workerApi,
	        Object executionStage) {
//			System.out.println(SystemTime.get().getCurrentDate()+ " Send starts");

			ArrayWritable<SIFTDataObject> new_dataObjects = workerApi.getAggregatedValue(MasterBlock.NEW_VERTEX_BROADCAST_KEY);


//			System.out.println(SystemTime.get().getCurrentDate()+ " Object loaded");

			long new_vertex_id = workerApi.getTotalNumVertices() + 1;

			System.out.println("--- InsertPiece ---");
			System.out.println(new_vertex_id);	     

			double thres;
			ArrayList<Double> thresList = thresholds.get();
			for (int i = 0; i < new_dataObjects.get().length; i++){  
				thres = thresList.get(i);
	        	for (int j = i +1; j < new_dataObjects.get().length; j++){
		    		double distance = ((SIFTFeatureList)new_dataObjects.get()[i].getFeatureList()).compareToQuery((SIFTFeatureList)new_dataObjects.get()[j].getFeatureList());
		    		
		    		if (distance < thres){
		    			DoubleWritable matches;	
		    			try{
		    				matches = new DoubleWritable (new_dataObjects.get()[i].getFeatureList().compareToInsert(new_dataObjects.get()[i].getFeatureList()));
		    			}
		    			catch(Exception e){
		    				matches = new DoubleWritable (0.0);
		    			}

			        	if(matches.get() > 100.0){
//			        		System.out.println("i " + (new_vertex_id + i));
//			        		System.out.println("j " + (new_vertex_id + j));
	
				        	workerApi.addEdgeRequest(new LongWritable(new_vertex_id + i), EdgeFactory.create(new LongWritable(new_vertex_id + j), matches));
				        	workerApi.addEdgeRequest(new LongWritable(new_vertex_id + j), EdgeFactory.create(new LongWritable(new_vertex_id + i), matches));
	
				        }
		    		}
	        	}
		    }
			return (vertex) -> {

			    if(vertex.getId().get() == 1){
			    	for (int i = 0; i < new_dataObjects.get().length; i++){
			    		workerApi.addVertexRequest(new LongWritable(new_vertex_id + i), new VertexValue(new_dataObjects.get()[i]));
//		        		System.out.println("vertex " + (new_vertex_id + i));
			        }
			    }
			    
		        ArrayList<Boolean> list = matchSupplier.get();
			    for (int i = 0; i < list.size(); i++){  
		        	if(list.get(i)){
		        		DoubleWritable matches;
		        		try{
		        			matches = new DoubleWritable (vertex.getValue().getFl().compareToInsert(new_dataObjects.get()[i].getFeatureList()));
		        		}
		        		catch(Exception e){
		    				matches = new DoubleWritable (0.0);
		    			}
				        if(matches.get() > 100.0){
				        	vertex.addEdge(EdgeFactory.create(new LongWritable(new_vertex_id + i), matches));
				        	workerApi.addEdgeRequest(new LongWritable(new_vertex_id + i), EdgeFactory.create(vertex.getId(), matches));
				        }
			        }
			    }
		    
	      };
	    }
	
//	public VertexReceiver<LongWritable, VertexValue, DoubleWritable, NoMessage> getVertexReceiver(
//    BlockWorkerReceiveApi<LongWritable> workerApi, Object executionStage) {
//  return new InnerVertexReceiver() {
//
//		@Override
//		public void vertexReceive(Vertex<LongWritable, VertexValue, DoubleWritable> vertex,
//				Iterable<NoMessage> arg1) {
//						
//			for (Edge  <LongWritable, DoubleWritable> e : vertex.getEdges()){
//				System.out.println(vertex.getId() + ":" + System.lineSeparator() +
//				e.getTargetVertexId()+ " " + e.getValue() + System.lineSeparator() + System.lineSeparator());
//			}
//		}
//  	
////  };
//}
	
}
