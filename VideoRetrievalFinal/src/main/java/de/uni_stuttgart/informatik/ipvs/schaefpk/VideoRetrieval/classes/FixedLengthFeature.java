package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

abstract public class FixedLengthFeature extends Feature {

	@Override
	abstract public void readFields(DataInput arg0) throws IOException;

	@Override
	abstract public void write(DataOutput arg0) throws IOException;

}
