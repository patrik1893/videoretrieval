package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.util.List;

import org.apache.hadoop.conf.Configuration;

public interface DataObjectProvider {

	public DataObject[] getObjects(Configuration conf);
	
	public boolean dataAvailable();
}
