package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.IOException;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;

public class BinaryColorLayoutFeature extends BinaryFixedLengthFeature {
	
	public static final String KEY = "BColorLayout";
	
	private BinaryColorLayoutFeature(){
		
	}
	
	public BinaryColorLayoutFeature(double[] array) {
		super(array);
		// TODO Auto-generated constructor stub
	}

	public BinaryColorLayoutFeature(DoubleFV vector) {
		super(vector);
		// TODO Auto-generated constructor stub
	}

	static public BinaryColorLayoutFeature read (DataInput input) throws IOException{
		BinaryColorLayoutFeature feature = new BinaryColorLayoutFeature();
		feature.readFields(input);
		return feature;
	}




}
