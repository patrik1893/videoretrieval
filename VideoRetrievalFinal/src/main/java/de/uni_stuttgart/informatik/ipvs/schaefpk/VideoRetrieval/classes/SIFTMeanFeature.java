package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.LinkedList;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.math.matrix.MeanVector;

public class SIFTMeanFeature extends Feature {

	DoubleFV vector;
	
	
	public SIFTMeanFeature(){
		
		vector = new DoubleFV(0);
	}
	
	
	public SIFTMeanFeature(LinkedList<Keypoint> list){
	
		MeanVector mv = new MeanVector();
		for(Keypoint k: list){
			mv.update(k.getFeatureVector().asDoubleVector());
//			System.out.println(k.getFeatureVector().toString());
//			System.out.println(new DoubleFV(mv.vec()).toString());
		}
		vector = new DoubleFV(mv.vec());
	}
	
	@Override
	public void readFields(DataInput arg0) throws IOException {
		
		vector.readBinary(arg0);

	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		vector.writeBinary(arg0);

	}

	@Override
	public double compareTo(Feature otherFeature) {
		// TODO Auto-generated method stub
		return vector.compare(((SIFTMeanFeature) otherFeature).getVector(), DoubleFVComparison.EUCLIDEAN);
	}

	public DoubleFV getVector() {
		return vector;
	}

	public void setVector(DoubleFV vector) {
		this.vector = vector;
	}
	
}
