package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import org.apache.giraph.block_app.framework.api.BlockWorkerReceiveApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerSendApi;
import org.apache.giraph.block_app.framework.piece.Piece;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexReceiver;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexSender;
import org.apache.giraph.edge.Edge;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.QueryMessage;

public class IterationPiece extends Piece<LongWritable, VertexValue, DoubleWritable, QueryMessage, Object>  {

	double alpha;
	double threshold;
	
	public IterationPiece(double alpha, double threshold)
	{
		this.alpha = alpha;
		this.threshold = threshold;
	}

@Override
public VertexSender<LongWritable, VertexValue, DoubleWritable> getVertexSender(
  final BlockWorkerSendApi<LongWritable, VertexValue, DoubleWritable,
    QueryMessage> workerApi,
  Object executionStage) {

	QueryMessage message = new QueryMessage();

		return (vertex) -> {
	  
//			double sum = 0.0;
////			for (Edge<LongWritable, DoubleWritable> e : vertex.getEdges()){
////				sum += Math.sqrt(e.getValue().get());
////			}
			
			for (int i = 0; i < vertex.getValue().getRanks().size(); i++){
				if(vertex.getValue().getRanks().get(i) > threshold ){
				  	for (Edge<LongWritable, DoubleWritable> e : vertex.getEdges()){
				
				
					    message.setQueryID(i);				    
	//				    message.setValue(vertex.getValue().getRanks().get(i)*(Math.sqrt(e.getValue().get())/sum));
	
					    message.setValue(vertex.getValue().getRanks().get(i)/vertex.getNumEdges());
				  		workerApi.sendMessage(e.getTargetVertexId(), message); 
				  	}
				}
			}
//	for (Edge<LongWritable, DoubleWritable> e : vertex.getEdges()){
//		
//  		message.set(vertex.getValue().getRank().get()/vertex.getNumEdges());
//  		workerApi.sendMessage(e.getTargetVertexId(), message); 
//  	}
		};
	}

	@Override
	public VertexReceiver<LongWritable, VertexValue, DoubleWritable, QueryMessage>
	getVertexReceiver(BlockWorkerReceiveApi<LongWritable> workerApi, Object executionStage) {
	
		return (vertex, messages) -> {
		  double[] sums = new double[(vertex.getValue().getRanks().size())];
		  for (QueryMessage value : messages) {
		    sums[value.getQueryID()] += value.getValue();
		  } 
		  for (int i = 0; i < sums.length; i++)
		  {
			  double newValue = alpha* vertex.getValue().getInitialSeedValues().get(i) + (1- alpha) * sums[i];
		      vertex.getValue().getRanks().set(i, newValue);
		  }
		};
	}

	@Override
	protected Class<QueryMessage> getMessageClass() {
		// TODO Auto-generated method stub
		return QueryMessage.class;
	}

	
}
