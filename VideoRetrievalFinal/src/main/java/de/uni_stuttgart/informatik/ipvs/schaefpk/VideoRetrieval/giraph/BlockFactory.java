package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import org.apache.giraph.block_app.framework.AbstractBlockFactory;
import org.apache.giraph.block_app.framework.block.Block;
import org.apache.giraph.block_app.framework.block.IfBlock;
import org.apache.giraph.block_app.framework.block.RepeatUntilBlock;
import org.apache.giraph.block_app.framework.block.SequenceBlock;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.function.ObjectTransfer;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
 
public class BlockFactory
  extends AbstractBlockFactory<Object>
{
  public Block createBlock(GiraphConfiguration conf)
  {
    ObjectTransfer<Boolean> insert = new ObjectTransfer();
    ObjectTransfer<Boolean> shutdown = new ObjectTransfer();
    ObjectTransfer<Integer> batchSize = new ObjectTransfer();
    QueryBlockFactory f = new QueryBlockFactory();
    InsertBlockFactory ibf = new InsertBlockFactory();
    MasterBlock mb = new MasterBlock(conf, insert, shutdown, batchSize);
    return new RepeatUntilBlock(
      Integer.MAX_VALUE, 
      new SequenceBlock(new Block[] {
      mb, 
      new IfBlock(insert, ibf.createBlock(conf, batchSize), f.createBlock(conf, batchSize)) }), 
      shutdown);
  }


  public Object createExecutionStage(GiraphConfiguration arg0)
  {
    return new Object();
  }
  
  protected Class<? extends Writable> getEdgeValueClass(GiraphConfiguration arg0)
  {
    return DoubleWritable.class;
  }
  
  protected Class<? extends WritableComparable> getVertexIDClass(GiraphConfiguration arg0)
  {
    return LongWritable.class;
  }
  

  protected Class<? extends Writable> getVertexValueClass(GiraphConfiguration arg0)
  {
    return VertexValue.class;
  }
}
