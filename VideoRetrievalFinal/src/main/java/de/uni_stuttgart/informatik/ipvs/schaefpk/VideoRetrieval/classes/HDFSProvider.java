package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSProvider implements DataObjectProvider {

	long pos;
	String path;
	boolean available;
	int batchsize;
	
	public HDFSProvider(Configuration conf) {
		path = conf.get("inputpath");
		batchsize = conf.getInt("batchsize", 10);
		available = true;
		pos = 0;
	}



	@Override
	public boolean dataAvailable() {
		return available;
	}



	@Override
	public DataObject[] getObjects(Configuration conf) {
		// TODO Auto-generated method stub
		try {
		    FileSystem fs = FileSystem.get(conf);
		    
			FSDataInputStream input = fs.open(new Path(conf.get("inputpath")));
			ArrayList<SIFTDataObject> list = new ArrayList<SIFTDataObject>(batchsize);
			
			SIFTDataObject siftobject;
			
			if (pos == 0){
				siftobject = SIFTDataObject.read(input);
				System.out.println(siftobject.getName() + " A");
			}
			else{
				input.seek(pos);
				siftobject = SIFTDataObject.read(input);

				System.out.println(siftobject.getName() + " B");
			}
			list.add(siftobject);
			
			if (input.available()>0){
				available = true;
			}
			else{
				available = false;
			}
			
			for (int i = 1; i < batchsize && available; i++){
				siftobject = SIFTDataObject.read(input);
				list.add(siftobject);
				if (input.available()>0){
					available = true;
				}
				else{
					available = false;
				}
			}
			pos = input.getPos();

			SIFTDataObject arr[] = new SIFTDataObject[list.size()];
			arr = list.toArray(arr);
			return arr;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
