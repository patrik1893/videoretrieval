package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;

import org.apache.giraph.block_app.framework.BlockUtils;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.conf.GiraphConstants;
import org.apache.giraph.conf.StrConfOption;
import org.apache.giraph.io.formats.FileOutputFormatUtil;
import org.apache.giraph.io.formats.GiraphFileInputFormat;
import org.apache.giraph.io.formats.JsonBase64VertexInputFormat;
import org.apache.giraph.io.formats.JsonBase64VertexOutputFormat;
import org.apache.giraph.job.GiraphJob;
import org.apache.giraph.utils.TestGraph;
import org.apache.giraph.yarn.GiraphYarnClient;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import nu.pattern.OpenCV;

public class GiraphStarter implements Tool {
	
	  static {
		    Configuration.addDefaultResource("giraph-site.xml");
		  }

	  private static final Logger LOG =
		      Logger.getLogger(GiraphStarter.class);

	  private Configuration conf;

	  
	@Override
	public Configuration getConf() {
		// TODO Auto-generated method stub
		return conf;
	}

	@Override
	public void setConf(Configuration arg0) {
		conf = arg0;
	}

	@Override
	public int run(String[] args) throws Exception {
		
		OpenCV.loadShared() ; 
		
		if (null == getConf()) { // for YARN profile
		      conf = new Configuration();
		    }
		GiraphConfiguration giraphConf = new GiraphConfiguration(getConf());

		String user = "ubuntu";

	    giraphConf.setDouble("alpha", Double.parseDouble(args[0]));
	    giraphConf.setInt("topn", Integer.parseInt(args[1]));  
	    giraphConf.setDouble("threshold", Double.parseDouble(args[2]));	 
//	    giraphConf.setInt("personal_page_rank.num_iterations", Integer.parseInt(args[2]));	    
		
//		GiraphConstants.CHECKPOINT_DIRECTORY.set(giraphConf, "/user/ubuntu/_bsp/_checkpoints/giraph_yarn_application_1484509684951_0001");
//		GiraphConstants.RESTART_JOB_ID.set(giraphConf, "application_1484509684951_0001");
//		giraphConf.setLong(
//				GiraphConstants.RESTART_SUPERSTEP,
//	             7800);
//		giraphConf.setNumComputeThreads(2);
		giraphConf.setWorkerConfiguration(16, 16, 100.0f);
//		giraphConf.setCheckpointFrequency(200);
		
//		giraphConf.setYarnLibJars("VideoRetrieval-0.0.1-SNAPSHOT.jar");
		
		// INPUT FORMAT AND PATH
	    
		GiraphConstants.VERTEX_INPUT_FORMAT_CLASS.set(giraphConf, JsonBase64VertexInputFormat.class);

	    GiraphFileInputFormat.addVertexInputPath(giraphConf, new Path("/user/"+ user +"/input/vertexoutput"));
	    
	    GiraphConstants.METRICS_ENABLE.set(giraphConf, true);
//	    GiraphConstants.METRICS_DIRECTORY.set(giraphConf, "/user/"+ user +"/metrics-query");

		// OUTPUT FORMAT AND PATH

//		GiraphConstants.VERTEX_OUTPUT_FORMAT_CLASS.set(giraphConf, JsonBase64VertexOutputFormat.class);
//		
//		giraphConf.set(
//				             org.apache.hadoop.mapreduce.lib.output.FileOutputFormat.OUTDIR,
//				             "/user/"+ user +"/output/vertexoutput");
		
	    
	    // CONF PARAMETER
	    
	    giraphConf.set("inputpath", "/user/"+ user +"/input/201302queries.out");
	    giraphConf.set("initmode", "TOPN");
	    giraphConf.set("pagerankmode", "ITERATIONS");

	    giraphConf.setInt("batchsize", 10);
	    
	    giraphConf.setDouble("threshold", 0.1);	    
	    
	    giraphConf.setYarnTaskHeapMb(8192);
	    
//	    GiraphConstants.USE_OUT_OF_CORE_GRAPH.set(giraphConf, true);
//	    GiraphConstants.MAX_PARTITIONS_IN_MEMORY.set(giraphConf, 1);
//	    GiraphConstants.USER_PARTITION_COUNT.set(giraphConf, 10);
	    
	    // SET AND INIT BLOCK FACTORYS
		 
		System.out.println("Init Factory"); 
	    
		 BlockUtils.setAndInitBlockFactoryClass(giraphConf, BlockFactory.class);
		 
	
		 // STARTING JOB
		 
		 System.out.println("Factory initalized. Starting GiraphYarnJob"); 
		 
		 GiraphYarnClient job = new GiraphYarnClient(giraphConf, "YARN-Test");
		 
		 return job.run(true) ? 0 : -1;
	}
	
	  public static void main(final String[] args) throws Exception {
		    System.exit(ToolRunner.run(new GiraphStarter(), args));
		  }
	

}
