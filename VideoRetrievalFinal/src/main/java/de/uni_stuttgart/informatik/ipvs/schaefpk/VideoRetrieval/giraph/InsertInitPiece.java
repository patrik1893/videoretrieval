package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import org.apache.giraph.block_app.framework.api.BlockMasterApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerReceiveApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerSendApi;
import org.apache.giraph.block_app.framework.api.CreateReducersApi;
import org.apache.giraph.block_app.framework.piece.Piece;
import org.apache.giraph.block_app.framework.piece.global_comm.ReducerAndBroadcastWrapperHandle;
import org.apache.giraph.block_app.framework.piece.global_comm.ReducerHandle;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexReceiver;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexSender;
import org.apache.giraph.block_app.reducers.TopNReduce;
import org.apache.giraph.function.Consumer;
import org.apache.giraph.function.Supplier;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.reducers.impl.SumReduce;
import org.apache.giraph.types.NoMessage;
import org.apache.giraph.utils.ArrayWritable;
import org.apache.giraph.writable.kryo.KryoWritableWrapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DoubleLong;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTFeatureList;

public class InsertInitPiece extends Piece<LongWritable, VertexValue, DoubleWritable, NoMessage, Object> {

    int n;
    Supplier<Integer> batchsizeSupplier;
    Consumer<ArrayList<Double>> thresholds;

    int batchsize;
    private Consumer<ArrayList<Boolean>> matchConsumer;

    private ArrayList<ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>> handles ;
    
    public InsertInitPiece(int n, Consumer<ArrayList<Boolean>> matchConsumer, Consumer<ArrayList<Double>> thresholds, Supplier<Integer> batchSize) {
		this.batchsizeSupplier = batchSize;
    	this.n = n;
    	this.matchConsumer = matchConsumer;
    	this.thresholds = thresholds;
    	handles = new ArrayList<ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>>();
    }

    @Override
    public void registerReducers(CreateReducersApi reduceApi, Object executionStage) {
    	ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>> handle;
    	handles = new ArrayList<ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>>();

    	batchsize = batchsizeSupplier.get();
    	for(int i = 0; i < batchsize; i++){
    		handle = new ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>();
    		handle.registeredReducer(reduceApi.createLocalReducer(new TopNReduce<DoubleLong>(n)));
    		handles.add(handle);
    	}
    }

    @Override
    public VertexSender<LongWritable, VertexValue, DoubleWritable>
    getVertexSender(

	 	        BlockWorkerSendApi<LongWritable, VertexValue, DoubleWritable, NoMessage> workerApi,
        Object executionStage) {
    	
//    	System.out.println("Insert Init");

		ArrayWritable<SIFTDataObject> new_DataObjects = workerApi.getAggregatedValue(MasterBlock.NEW_VERTEX_BROADCAST_KEY);

		return (vertex) -> {

    	  for (int i = 0; i <new_DataObjects.get().length; i++){
//    		  System.out.println(i);
//    		  System.out.println(new_DataObjects.get()[i].getName());
//    		  System.out.println(new_DataObjects.get()[i].getFeatureList() == null);
//    		  System.out.println(vertex.getValue().getName());
//    		  System.out.println(vertex.getValue().getFl() == null);
    		  double distance = ((SIFTFeatureList)new_DataObjects.get()[i].getFeatureList()).compareToQuery(vertex.getValue().getFl());

    		  handles.get(i).reduce(new DoubleLong(distance, vertex.getId().get(), vertex.getValue().getName()));
    	  }
      };
    }
    
    public void masterCompute(BlockMasterApi masterApi, Object executionStage)
    {
    	for (int i = 0; i < handles.size(); i++){
    		handles.get(i).broadcastValue(masterApi);
    	}
    }
    
    public VertexReceiver<LongWritable, VertexValue, DoubleWritable, NoMessage> getVertexReceiver(
            BlockWorkerReceiveApi<LongWritable> workerApi, Object executionStage) {
    		ArrayList<HashSet<Long>> setList = new ArrayList<HashSet<Long>>(handles.size());
    		ArrayList<Double> thresList = new ArrayList<Double>();
    		for (int j = 0; j < handles.size(); j ++){
	           PriorityQueue<DoubleLong> pq = handles.get(j).getBroadcast(workerApi).get();
	           
	           if (pq.size() < n){
	        	   thresList.add(Double.MAX_VALUE);
	           }else{
	        	   
	           }
	           
	           HashSet<Long> set = new HashSet<Long>(pq.size());
	           int size = pq.size();
	           
	           DoubleLong dl = pq.poll();
	           set.add(dl.getVertex_id());
	           thresList.add(dl.getDist());
		       for (int i = 1; i < size; i++){
		        		//        	   System.out.println("TopN " + pq.peek().getVertex_id());
		        	set.add(pq.poll().getVertex_id());
		       }
	           setList.add(set);
    		}
    		
    		thresholds.apply(thresList);
          return new InnerVertexReceiver() {
            @Override
            public void vertexReceive(
                Vertex<LongWritable, VertexValue, DoubleWritable> vertex, Iterable<NoMessage> messages) {
        		ArrayList<Boolean> bools = new ArrayList<Boolean>();
            	for (int j = 0; j < handles.size(); j ++){

	            	if(setList.get(j).contains(vertex.getId().get())){
	            		bools.add(true);
	            	}else{
	            		bools.add(false);
	            	}
        		}
            	matchConsumer.apply(bools);
            }

          };
     }

}
