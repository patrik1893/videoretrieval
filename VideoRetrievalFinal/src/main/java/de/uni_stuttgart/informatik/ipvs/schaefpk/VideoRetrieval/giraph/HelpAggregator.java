package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.util.ArrayList;

import org.apache.giraph.aggregators.BasicAggregator;
import org.apache.giraph.utils.ArrayListWritable;
import org.apache.giraph.utils.ArrayWritable;
import org.apache.hadoop.io.DoubleWritable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.BinaryDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTDataObject;

public class HelpAggregator extends BasicAggregator<ArrayWritable<SIFTDataObject>> {

	@Override
	public void aggregate(ArrayWritable<SIFTDataObject> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayWritable<SIFTDataObject> createInitialValue() {
		// TODO Auto-generated method stub
		
		SIFTDataObject [] initial = new SIFTDataObject[0];
		return new ArrayWritable<SIFTDataObject>(SIFTDataObject.class, initial);
	}

}
