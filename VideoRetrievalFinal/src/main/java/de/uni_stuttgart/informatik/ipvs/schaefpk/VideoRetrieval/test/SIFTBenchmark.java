
package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.openimaj.feature.local.list.FileLocalFeatureList;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.list.MemoryLocalFeatureList;
import org.openimaj.feature.local.matcher.BasicTwoWayMatcher;
import org.openimaj.feature.local.matcher.FastBasicKeypointMatcher;
import org.openimaj.feature.local.matcher.FastEuclideanKeypointMatcher;
import org.openimaj.feature.local.matcher.LocalFeatureMatcher;
import org.openimaj.feature.local.matcher.MatchingUtilities;
import org.openimaj.feature.local.matcher.VotingKeypointMatcher;
import org.openimaj.feature.local.matcher.consistent.ConsistentLocalFeatureMatcher2d;
import org.openimaj.feature.local.matcher.consistent.LocalConsistentKeypointMatcher;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.feature.local.engine.DoGColourSIFTEngine;
import org.openimaj.image.feature.local.engine.DoGSIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ASIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ColourASIFTEngine;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.transforms.HomographyModel;
import org.openimaj.math.geometry.transforms.HomographyRefinement;
import org.openimaj.math.geometry.transforms.estimation.RobustAffineTransformEstimator;
import org.openimaj.math.geometry.transforms.estimation.RobustHomographyEstimator;
import org.openimaj.math.geometry.transforms.residuals.SingleImageTransferResidual2d;
import org.openimaj.math.model.fit.RANSAC;
import org.openimaj.util.pair.Pair;

public class SIFTBenchmark {
	
	public static void main(String[] args) throws IOException {
		
        String bookObject = "images/bookobject.jpg";
        String bookScene = "images/bookscene.jpg";

        FImage img1 = ImageUtilities.readF(new File(bookObject));

        FImage img2 = ImageUtilities.readF(new File(bookScene));
		DoGSIFTEngine engine = new DoGSIFTEngine();

		List<Keypoint> points1 = engine.findFeatures(img1);
		System.out.println(points1.size());
		List<Keypoint> points2 = engine.findFeatures(img2);
		System.out.println(points2.size());
		
		long starttime = System.currentTimeMillis();
//		RobustAffineTransformEstimator modelFitter = new RobustAffineTransformEstimator(10.0, 1000,
//				  new RANSAC.BestFitStoppingCondition());
//		RobustHomographyEstimator modelFitter = new RobustHomographyEstimator(1.0, 1000, new RANSAC.BestFitStoppingCondition(),
//				HomographyRefinement.NONE);

//		LocalFeatureMatcher<Keypoint>	matcher = new ConsistentLocalFeatureMatcher2d<Keypoint>(
//				  new FastBasicKeypointMatcher<Keypoint>(8), modelFitter);
		
		LocalFeatureMatcher<Keypoint>	matcher = new FastBasicKeypointMatcher<Keypoint>(8);

		matcher.setModelFeatures(points2);
		
		System.out.println("KD tree " + (System.currentTimeMillis() - starttime));
		
		matcher.findMatches(points1);
		
		System.out.println("matched " + (System.currentTimeMillis() - starttime));

		
	}

	static void match (LocalFeatureMatcher<Keypoint> matcher, LocalFeatureList<Keypoint> f1, LocalFeatureList<Keypoint> f2)
	{
		if (f1.size() < f2.size()){
			matcher.setModelFeatures(f1);
			matcher.findMatches(f2);
			System.out.println(matcher.getMatches().size() + "  -  " + f1.size() * 0.03);	
		}
		else{
			matcher.setModelFeatures(f2);
			matcher.findMatches(f1);			
			System.out.println(matcher.getMatches().size() + "  -  " + f2.size() * 0.03);	
		}
	}
	
	/**
	 * @return a matcher with a homographic constraint
	 */
	private static LocalFeatureMatcher<Keypoint> createConsistentRANSACHomographyMatcher() {
		final ConsistentLocalFeatureMatcher2d<Keypoint> matcher = new ConsistentLocalFeatureMatcher2d<Keypoint>(
				createFastBasicMatcher());
		matcher.setFittingModel(new RobustHomographyEstimator(1.0, 10, new RANSAC.BestFitStoppingCondition(),
				HomographyRefinement.NONE   ));

		return matcher;
	}

	/**
	 * @return a basic matcher
	 */
	private static LocalFeatureMatcher<Keypoint> createFastBasicMatcher() {
		return new FastBasicKeypointMatcher<Keypoint>(8);
	}
}