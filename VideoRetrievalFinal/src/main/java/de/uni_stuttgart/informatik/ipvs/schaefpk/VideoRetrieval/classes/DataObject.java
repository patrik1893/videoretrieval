package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

abstract public class DataObject implements Writable {
	
	public static final String QUERY_KEY = "QUERY";
	public static final String INSERT_KEY = "INSERT";
	
	String type;
	String name;
	FeatureList featureList;
	
	protected DataObject(){
		
	}
	
	protected DataObject(String type, String name, FeatureList featureList){
		this.type = type;
		this.name =name;
		this.featureList = featureList;
	}
	@Override
	abstract public void readFields(DataInput dataInput) throws IOException;
	
	@Override
	abstract public void write(DataOutput dataOutput) throws IOException;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FeatureList getFeatureList() {
		return featureList;
	}

	public void setFeatureList(FeatureList featureList) {
		this.featureList = featureList;
	}
	
	
}
