package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import org.apache.giraph.block_app.framework.api.BlockMasterApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerReceiveApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerSendApi;
import org.apache.giraph.block_app.framework.api.CreateReducersApi;
import org.apache.giraph.block_app.framework.piece.Piece;
import org.apache.giraph.block_app.framework.piece.global_comm.ReducerAndBroadcastWrapperHandle;
import org.apache.giraph.block_app.framework.piece.global_comm.ReducerHandle;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexReceiver;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexSender;
import org.apache.giraph.block_app.reducers.TopNReduce;
import org.apache.giraph.function.ObjectTransfer;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.reducers.impl.SumReduce;
import org.apache.giraph.types.NoMessage;
import org.apache.giraph.writable.kryo.KryoWritableWrapper;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.BinaryDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DoubleLong;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DoubleText;

public class GatherResultPiece extends Piece<LongWritable, VertexValue, DoubleWritable, NoMessage, Object> {

    int n;
    ObjectTransfer<Integer> batchSizeTransfer;
    int batchSize;
    private ArrayList<ReducerAndBroadcastWrapperHandle<DoubleText, KryoWritableWrapper<PriorityQueue<DoubleText>>>> handles ;
;
    
    public GatherResultPiece(int n, ObjectTransfer<Integer> batchSize) {
    	this.n = n;
    	this.batchSizeTransfer = batchSize;
    }

    @Override
    public void registerReducers(CreateReducersApi reduceApi, Object executionStage) {
    	ReducerAndBroadcastWrapperHandle<DoubleText, KryoWritableWrapper<PriorityQueue<DoubleText>>> handle;
    	batchSize = batchSizeTransfer.get();
    	handles = new ArrayList<ReducerAndBroadcastWrapperHandle<DoubleText, KryoWritableWrapper<PriorityQueue<DoubleText>>>>();
    	for(int i = 0; i < batchSize; i++){
    		handle = new ReducerAndBroadcastWrapperHandle<DoubleText, KryoWritableWrapper<PriorityQueue<DoubleText>>>();
    		handle.registeredReducer(reduceApi.createLocalReducer(new TopNReduce<DoubleText>(n)));
    		handles.add(handle);
    	}   
    }

    @Override
    public VertexSender<LongWritable, VertexValue, DoubleWritable>
    getVertexSender(

	 	        BlockWorkerSendApi<LongWritable, VertexValue, DoubleWritable, NoMessage> workerApi,
        Object executionStage) {
    	
      return (vertex) -> {
    	  for (int i = 0; i < vertex.getValue().getRanks().size(); i++){
//        	  System.out.println(vertex.getValue().getName() + " " + vertex.getValue().getRanks().get(i));
    	      handles.get(i).reduce(new DoubleText(vertex.getValue().getRanks().get(i), vertex.getValue().getName()));
    	  }
      };
    }
    
    public void masterCompute(BlockMasterApi masterApi, Object executionStage)
    {
    	DoubleText dl;

    	for (int j = 0; j < handles.size(); j++){
            PriorityQueue<DoubleText> pq = handles.get(j).getReducedValue(masterApi).get();
            System.out.println("RESULT "   + j + " PQ: " + pq.size());
            int pqsize = pq.size();
            for (int i = 0; i < pqsize; i++){
         	   dl = pq.poll();
         	   System.out.println(dl.getDist() + " " + dl.getName());
            }
 		}
    }
    
}
