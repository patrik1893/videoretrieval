package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.BinaryDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DataObjectProvider;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.HDFSProvider;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTDataObject;

import java.io.PrintStream;
import java.util.ArrayList;

import org.apache.giraph.block_app.framework.api.BlockMasterApi;
import org.apache.giraph.block_app.framework.piece.Piece;
import org.apache.giraph.function.Consumer;
import org.apache.giraph.time.SystemTime;
import org.apache.giraph.types.NoMessage;
import org.apache.giraph.utils.ArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.yarn.util.Times;

public class MasterBlock
  extends Piece<LongWritable, VertexValue, DoubleWritable, NoMessage, Object>
{
  public static String NEW_VERTEX_BROADCAST_KEY = "NEW_VERTEX";
  private DataObjectProvider provider;
  private final Consumer<Boolean> insert;
  private final Consumer<Boolean> shutdown;
  private final Consumer<Integer> batchSize;
  private int counter;
  
  public MasterBlock(Configuration conf, Consumer<Boolean> insert, Consumer<Boolean> shutdown, Consumer<Integer> batchSize)
  {
    System.out.println("MasterBlock constructed");
    counter = 0;
    this.provider = new HDFSProvider(conf);
    this.insert = insert;
    this.shutdown = shutdown;
    this.batchSize = batchSize;
  }
  
  public void registerAggregators(BlockMasterApi masterApi)
    throws InstantiationException, IllegalAccessException
  {
    masterApi.registerPersistentAggregator(NEW_VERTEX_BROADCAST_KEY, HelpAggregator.class);
  }
  
  public void masterCompute(BlockMasterApi masterApi, Object executionStage)
  {
    System.out.println(SystemTime.get().getCurrentDate()+ " Master Compute starts Batch " + counter++);
    
    
    if (this.provider.dataAvailable())
    {
    	SIFTDataObject [] arr = (SIFTDataObject [])this.provider.getObjects(masterApi.getConf());

    	batchSize.apply(arr.length);
      System.out.println(SystemTime.get().getCurrentDate()+ "Objects exist and are loaded");
      masterApi.setAggregatedValue(NEW_VERTEX_BROADCAST_KEY, new ArrayWritable<SIFTDataObject>(SIFTDataObject.class, arr));
      if (arr[0].getType().compareTo("INSERT") == 0) {
        this.insert.apply(Boolean.valueOf(true));
      } else {
        this.insert.apply(Boolean.valueOf(false));
      }
      this.shutdown.apply(Boolean.valueOf(false));
    }
    else
    {
      System.out.println("Object not exist");
      this.shutdown.apply(Boolean.valueOf(true));
    }
  }
}
