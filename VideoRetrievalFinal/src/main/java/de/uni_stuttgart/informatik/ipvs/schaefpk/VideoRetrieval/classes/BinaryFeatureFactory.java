package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.IOException;

public class BinaryFeatureFactory {
	
	static public Feature getFeature(String featureClass, DataInput input) throws IOException {
		
		switch (featureClass) {
		
		
		case BinaryPHOGFeature.KEY    :  return BinaryPHOGFeature.read (input);
		case BinaryScalableColorFeature.KEY    :  return BinaryScalableColorFeature.read (input);
		case BinaryEdgeHistogramFeature.KEY    :  return BinaryEdgeHistogramFeature.read (input);
		case BinaryColorLayoutFeature.KEY    :  return BinaryColorLayoutFeature.read (input);
		
		}
		
		throw new IOException ("Feature not known");
    }
}