package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.feature_extraction;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class KeyframeProvider {

    public static ArrayList<File> getFileList(String verzName) {
        ArrayList<File> files = getPaths(new File(verzName),
                new ArrayList<File>());
        return files;
    }

    private static ArrayList<File> getPaths(File file, ArrayList<File> list) {
        if (file == null || list == null || !file.isDirectory())
            return null;
        File[] fileArr = file.listFiles();
        for (File f : fileArr) {
            if (f.isDirectory()) {
                getPaths(f, list);
            }
            if (f.getName().endsWith("jpg")){
                list.add(f);
            }
        }
        return list;
    }
} 