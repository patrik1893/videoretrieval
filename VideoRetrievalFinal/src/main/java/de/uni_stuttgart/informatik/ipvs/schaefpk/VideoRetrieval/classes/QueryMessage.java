package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class QueryMessage implements Writable {

	int queryID;
	double value;
	
	public QueryMessage() {
	}
	
	public QueryMessage(int queryID, double value) {
		super();
		this.queryID = queryID;
		this.value = value;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		queryID = in.readInt();
		value = in.readDouble();

	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(queryID);
		out.writeDouble(value);
	}

	public int getQueryID() {
		return queryID;
	}

	public void setQueryID(int queryID) {
		this.queryID = queryID;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	
}
