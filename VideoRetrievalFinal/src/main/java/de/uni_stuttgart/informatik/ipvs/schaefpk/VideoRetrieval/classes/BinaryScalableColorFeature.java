package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.IOException;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;

public class BinaryScalableColorFeature extends BinaryFixedLengthFeature {
	
	public static final String KEY = "BScalableColor";
	
	private BinaryScalableColorFeature(){
		
	}
	
	
	
	public BinaryScalableColorFeature(double[] array) {
		super(array);
		// TODO Auto-generated constructor stub
	}



	public BinaryScalableColorFeature(DoubleFV vector) {
		super(vector);
		// TODO Auto-generated constructor stub
	}



	static public BinaryScalableColorFeature read (DataInput input) throws IOException{
		BinaryScalableColorFeature feature = new BinaryScalableColorFeature();
		feature.readFields(input);
		return feature;
	}




}
