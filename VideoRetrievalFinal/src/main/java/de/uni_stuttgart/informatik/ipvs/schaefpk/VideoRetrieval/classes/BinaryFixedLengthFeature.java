package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;

abstract public class BinaryFixedLengthFeature extends FixedLengthFeature {
		
	protected DoubleFV vector;
	
	public BinaryFixedLengthFeature() {
		
	}
	
	protected BinaryFixedLengthFeature(DoubleFV vector) {
		this.vector = vector;
	}
	
	protected BinaryFixedLengthFeature(double [] array) {
		this.vector = new DoubleFV(array);
	}

	@Override
	public void readFields(DataInput input) throws IOException {
		vector = new DoubleFV();
		vector.readBinary(input);
		
	}

	@Override
	public void write(DataOutput output) throws IOException {
		vector.writeBinary(output);
		
	}

	public double compareTo(Feature otherFeature) {
		
		 
		return 1.0  - Math.abs(vector.compare(((BinaryFixedLengthFeature)otherFeature).vector, DoubleFVComparison.EUCLIDEAN));
	}

	public DoubleFV getVector() {
		return vector;
	}

	public void setVector(DoubleFV vector) {
		this.vector = vector;
	}
	
	
}
