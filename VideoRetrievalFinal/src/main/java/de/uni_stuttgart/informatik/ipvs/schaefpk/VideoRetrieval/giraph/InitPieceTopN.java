package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import org.apache.giraph.block_app.framework.api.BlockMasterApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerReceiveApi;
import org.apache.giraph.block_app.framework.api.BlockWorkerSendApi;
import org.apache.giraph.block_app.framework.api.CreateReducersApi;
import org.apache.giraph.block_app.framework.piece.Piece;
import org.apache.giraph.block_app.framework.piece.global_comm.ReducerAndBroadcastWrapperHandle;
import org.apache.giraph.block_app.framework.piece.global_comm.ReducerHandle;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexReceiver;
import org.apache.giraph.block_app.framework.piece.interfaces.VertexSender;
import org.apache.giraph.block_app.reducers.TopNReduce;
import org.apache.giraph.function.ObjectTransfer;
import org.apache.giraph.function.Supplier;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.reducers.impl.SumReduce;
import org.apache.giraph.types.NoMessage;
import org.apache.giraph.utils.ArrayWritable;
import org.apache.giraph.writable.kryo.KryoWritableWrapper;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DoubleLong;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTFeatureList;

public class InitPieceTopN extends Piece<LongWritable, VertexValue, DoubleWritable, NoMessage, Object> {

    int pqsize;
    ObjectTransfer<Integer> batchsizeTransfer;
    int batchsize;
    private ArrayList<ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>> handles ;

    public InitPieceTopN(int pqsize, ObjectTransfer<Integer> batchSize) {
    	this.pqsize = pqsize;
    	this.batchsizeTransfer = batchSize;
    	handles = new ArrayList<ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>>();
    }

    @Override
    public void registerReducers(CreateReducersApi reduceApi, Object executionStage) {
    	ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>> handle;
    	batchsize = batchsizeTransfer.get();
    	batchsizeTransfer.apply(batchsize);
    	handles = new ArrayList<ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>>();
    	for(int i = 0; i < batchsize; i++){
    		handle = new ReducerAndBroadcastWrapperHandle<DoubleLong, KryoWritableWrapper<PriorityQueue<DoubleLong>>>();
    		handle.registeredReducer(reduceApi.createLocalReducer(new TopNReduce<DoubleLong>(pqsize)));
    		handles.add(handle);
    	}    
    }

    @Override
    public VertexSender<LongWritable, VertexValue, DoubleWritable>
    getVertexSender(

	 	        BlockWorkerSendApi<LongWritable, VertexValue, DoubleWritable, NoMessage> workerApi,
        Object executionStage) {
    	
    	System.out.println("Query Init");

		ArrayWritable<SIFTDataObject> new_DataObjects = workerApi.getAggregatedValue(MasterBlock.NEW_VERTEX_BROADCAST_KEY);

		return (vertex) -> {
    	  	      
    	  for (int i = 0; i <new_DataObjects.get().length; i++){
    		  double distance = ((SIFTFeatureList)new_DataObjects.get()[i].getFeatureList()).compareToQuery(vertex.getValue().getFl());

    		  handles.get(i).reduce(new DoubleLong(distance, vertex.getId().get(), vertex.getValue().getName()));
    	  }
      };
    }
    
    public void masterCompute(BlockMasterApi masterApi, Object executionStage)
    {
    	for (int i = 0; i < handles.size(); i++){
    		handles.get(i).broadcastValue(masterApi);
    	}
    }
    
    public VertexReceiver<LongWritable, VertexValue, DoubleWritable, NoMessage> getVertexReceiver(
            BlockWorkerReceiveApi<LongWritable> workerApi, Object executionStage) {
    	ArrayList<HashSet<Long>> setList = new ArrayList<HashSet<Long>>(handles.size());
    	DoubleLong dl;
		for (int j = 0; j < handles.size(); j ++){
           PriorityQueue<DoubleLong> pq = handles.get(j).getBroadcast(workerApi).get();
           System.out.println("TopN "   + j + " PQ: " + pq.size());
           HashSet<Long> set = new HashSet<Long>(20);
//           int size = 20;
           int pqsize = pq.size();
           for (int i = 0; i < pqsize; i++){
        	   dl = pq.poll();        	   
        	   set.add(dl.getVertex_id()); 

        	   System.out.println(dl.getDist() + " " + dl.getName());
//        	   if ( i >= (pqsize - size)){
//            	   set.add(dl.getVertex_id());        		   
//        	   }
           }
           setList.add(set);
		}
		ArrayWritable<SIFTDataObject> new_DataObjects = workerApi.getAggregatedValue(MasterBlock.NEW_VERTEX_BROADCAST_KEY);

          return new InnerVertexReceiver() {
            @Override
            public void vertexReceive(
                Vertex<LongWritable, VertexValue, DoubleWritable> vertex, Iterable<NoMessage> messages) {

        		vertex.getValue().setInitialSeedValues(new ArrayList<Double>());
        		vertex.getValue().setRanks(new ArrayList<Double>());
            	for (int j = 0; j < handles.size(); j ++){

            		if(setList.get(j).contains(vertex.getId().get())){
                  	   DoubleWritable matches = new DoubleWritable (vertex.getValue().getFl().compareToInsert(new_DataObjects.get()[j].getFeatureList()));
         		        if(matches.get() > 20.0){
    	    	        	vertex.getValue().getInitialSeedValues().add(1.0);
    	    	        	vertex.getValue().getRanks().add(1.0);	
         		        }
          		       else{
      	    	        	vertex.getValue().getInitialSeedValues().add(0.0);
      	    	        	vertex.getValue().getRanks().add(0.0);
            		       }
	    	        }
	    	        else{
	    	        	vertex.getValue().getInitialSeedValues().add(0.0);
	    	        	vertex.getValue().getRanks().add(0.0);
	    	        }
            	}
            }

          };
     }

}
