package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.hadoop.io.Writable;

public class BinaryDataObject extends DataObject implements Writable {
	
	public BinaryDataObject(){
		type = new String("");
		name = new String("");
		ArrayList<Entry<String, Feature>>  fs = new ArrayList<Entry<String, Feature>> ();
		featureList = new BinaryFeatureList(fs);
	}

	public BinaryDataObject(String type, String name, StandardFeatureList featureList) {
		super(type, name, featureList);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void readFields(DataInput dataInput) throws IOException {
		type = dataInput.readUTF();
		name = dataInput.readUTF();
		featureList = BinaryFeatureList.read(dataInput);
	}

	@Override
	public void write(DataOutput dataOutput) throws IOException {
		if (type == null){
			throw new IOException("No type for DataObject specified");
		}
		else if (featureList == null){
			throw new IOException("No FeatureList for DataObject specified");
		}
		else if (name == null){
			throw new IOException("No Name for DataObject specified");
		}
		else{
			dataOutput.writeUTF(type);
			dataOutput.writeUTF(name);
			featureList.write(dataOutput);
		}
	}
	
    public static BinaryDataObject read(DataInput in) throws IOException {
    	System.out.println("Read BinaryDataObject");

    	BinaryDataObject dobj = new BinaryDataObject();
    	dobj.readFields(in);
    	return dobj;
      }

}
