package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.feature_extraction;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.openimaj.feature.local.list.FileLocalFeatureList;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.list.MemoryLocalFeatureList;
import org.openimaj.feature.local.matcher.BasicTwoWayMatcher;
import org.openimaj.feature.local.matcher.FastBasicKeypointMatcher;
import org.openimaj.feature.local.matcher.FastEuclideanKeypointMatcher;
import org.openimaj.feature.local.matcher.LocalFeatureMatcher;
import org.openimaj.feature.local.matcher.MatchingUtilities;
import org.openimaj.feature.local.matcher.VotingKeypointMatcher;
import org.openimaj.feature.local.matcher.consistent.ConsistentLocalFeatureMatcher2d;
import org.openimaj.feature.local.matcher.consistent.LocalConsistentKeypointMatcher;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.feature.local.engine.DoGColourSIFTEngine;
import org.openimaj.image.feature.local.engine.DoGSIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ASIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ColourASIFTEngine;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.transforms.HomographyModel;
import org.openimaj.math.geometry.transforms.HomographyRefinement;
import org.openimaj.math.geometry.transforms.estimation.RobustHomographyEstimator;
import org.openimaj.math.geometry.transforms.residuals.SingleImageTransferResidual2d;
import org.openimaj.math.model.fit.RANSAC;
import org.openimaj.util.pair.Pair;

public class SIFTExtractionI2VQuery {
	
	public static void main(String[] args) throws IOException {
		// Read the images from two streams
		ArrayList<File> files = KeyframeProvider.getFileList("/home/patrik/database/queries");

		DataOutputStream pw ;


		DoGSIFTEngine engine = new DoGSIFTEngine();
		for (int i = 0; i < files.size(); i++){
			String str = files.get(i).getCanonicalPath();
            str = str.substring(30, str.length()-4).replace('/', '_') + ".sift";
            if(str.contains("201302")){
				pw = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("/home/patrik/database/sift-queries/" + str)));
			
				engine.findFeatures(ImageUtilities.readF(files.get(i))).writeBinary(pw);
            
				System.out.println(str);
				pw.close();
	
            }
		}
	}
}