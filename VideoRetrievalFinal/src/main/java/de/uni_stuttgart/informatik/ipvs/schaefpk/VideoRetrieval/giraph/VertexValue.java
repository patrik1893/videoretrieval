package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.giraph;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.BinaryFeatureList;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.DataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.FeatureList;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTFeatureList;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.StandardFeatureList;

public class VertexValue implements Writable {
	
	String name;
	ArrayList<Double> initialSeedValues;
	ArrayList<Double> ranks;
	FeatureList fl;

	public VertexValue(DataObject obj){
		name = obj.getName();
		fl = obj.getFeatureList();
		initialSeedValues = new ArrayList<Double> ();
		ranks = new ArrayList<Double>();
	}
	
	public VertexValue(){
		
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		name = in.readUTF();
		fl = SIFTFeatureList.read(in);
		initialSeedValues = new ArrayList<Double> ();
		ranks = new ArrayList<Double>();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeUTF(name);
		fl.write(out);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FeatureList getFl() {
		return fl;
	}

	public void setFl(StandardFeatureList fl) {
		this.fl = fl;
	}

	public ArrayList<Double> getInitialSeedValues() {
		return initialSeedValues;
	}

	public void setInitialSeedValues(ArrayList<Double> initialSeedValues) {
		this.initialSeedValues = initialSeedValues;
	}

	public ArrayList<Double> getRanks() {
		return ranks;
	}

	public void setRanks(ArrayList<Double> ranks) {
		this.ranks = ranks;
	}

	
	

}
