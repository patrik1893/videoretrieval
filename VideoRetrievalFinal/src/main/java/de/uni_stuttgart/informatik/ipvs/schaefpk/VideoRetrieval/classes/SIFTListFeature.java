package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.KeyPoint;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.list.MemoryLocalFeatureList;
import org.openimaj.feature.local.matcher.FastBasicKeypointMatcher;
import org.openimaj.feature.local.matcher.LocalFeatureMatcher;
import org.openimaj.feature.local.matcher.consistent.ConsistentLocalFeatureMatcher2d;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.transforms.HomographyModel;
import org.openimaj.math.geometry.transforms.HomographyRefinement;
import org.openimaj.math.geometry.transforms.estimation.RobustAffineTransformEstimator;
import org.openimaj.math.geometry.transforms.estimation.RobustHomographyEstimator;
import org.openimaj.math.geometry.transforms.residuals.SingleImageTransferResidual2d;
import org.openimaj.math.model.fit.RANSAC;

import nu.pattern.OpenCV;


public class SIFTListFeature extends Feature {

	MatOfKeyPoint keypoints;
	Mat descriptors;
	int veclength = 128;
	
	public SIFTListFeature() {
		super();
		OpenCV.loadShared() ; 

		keypoints = new MatOfKeyPoint(); 
		descriptors = new Mat(); 
	}
	
	

	public SIFTListFeature(MatOfKeyPoint keypoints, Mat descriptors) {
		super();
		this.keypoints = keypoints;
		this.descriptors = descriptors;
	}



	public void readFields(DataInput in) throws IOException {
		
		int size = in.readInt();
		veclength = in.readInt();
		ArrayList<KeyPoint> keypointsCV = new ArrayList<KeyPoint>();
		KeyPoint kpCV;		
		keypoints = new MatOfKeyPoint(); 
		descriptors = new Mat(size, veclength, CvType.CV_8U);
		for(int i = 0; i < size; i++){
			Keypoint kp = new Keypoint(veclength);
			kp.readBinary(in);

			kpCV = new KeyPoint(kp.getX(), kp.getY(), kp.getScale(), kp.getLocation().getOrdinate(3));
			keypointsCV.add(kpCV);
			descriptors.put(i, 0, kp.ivec);
		}
		descriptors.convertTo(descriptors, CvType.CV_32F);
		keypoints.fromList(keypointsCV);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		ArrayList<KeyPoint> keypointsCV = new ArrayList<KeyPoint>(keypoints.toList());		
		
		
		out.writeInt(keypointsCV.size());
		out.writeInt(veclength);
		Keypoint kp;
		Mat outMat = new Mat();
		descriptors.convertTo(outMat, CvType.CV_8U);

		byte[] array = new byte[128];
		for(int i = 0; i < keypointsCV.size(); i++){
			
			outMat.row(i).get(0, 0, array);
			kp = new Keypoint((float) keypointsCV.get(i).pt.x, (float)keypointsCV.get(i).pt.y, keypointsCV.get(i).size, keypointsCV.get(i).angle, array);
			kp.writeBinary(out);
		}
	}

	@Override
	public double compareTo(Feature otherFeature) {
		
		
        List<MatOfDMatch> matches = new LinkedList<MatOfDMatch>();
        DescriptorMatcher descriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
//        System.out.println((descriptors.type() == CvType.CV_32F));
//        System.out.println((((SIFTListFeature) otherFeature).getDescriptors().type() == CvType.CV_32F));
//        System.out.println(descriptors.rows());
//        System.out.println((((SIFTListFeature) otherFeature).getDescriptors().rows()));
        descriptorMatcher.knnMatch(descriptors, ((SIFTListFeature) otherFeature).getDescriptors(), matches, 2);

        LinkedList<DMatch> goodMatchesList = new LinkedList<DMatch>();

        float nndrRatio = 0.7f;

        for (int i = 0; i < matches.size(); i++) {
            MatOfDMatch matofDMatch = matches.get(i);
            DMatch[] dmatcharray = matofDMatch.toArray();
            DMatch m1 = dmatcharray[0];
            DMatch m2 = dmatcharray[1];

            if (m1.distance <= m2.distance * nndrRatio) {
                goodMatchesList.addLast(m1);

            }
        }

//        System.out.println("Good Matches: " + goodMatchesList.size());
        
        if (goodMatchesList.size() > 100){

	        List<KeyPoint> objKeypointlist = keypoints.toList();
	        List<KeyPoint> scnKeypointlist = ((SIFTListFeature) otherFeature).getKeypoints().toList();
	
	        LinkedList<Point> objectPoints = new LinkedList<>();
	        LinkedList<Point> scenePoints = new LinkedList<>();
	
	        for (int i = 0; i < goodMatchesList.size(); i++) {
	            objectPoints.addLast(objKeypointlist.get(goodMatchesList.get(i).queryIdx).pt);
	            scenePoints.addLast(scnKeypointlist.get(goodMatchesList.get(i).trainIdx).pt);
	        }
	
	        MatOfPoint2f objMatOfPoint2f = new MatOfPoint2f();
	        objMatOfPoint2f.fromList(objectPoints);
	        MatOfPoint2f scnMatOfPoint2f = new MatOfPoint2f();
	        scnMatOfPoint2f.fromList(scenePoints);
	    	
	        Mat mask = new Mat();
	        Mat homography = Calib3d.findHomography(objMatOfPoint2f, scnMatOfPoint2f, Calib3d.RANSAC, 3, mask);
	        
	        int counter = 0;
	        for (int i = 0; i < mask.rows(); i++){
	        	if (mask.get(i, 0)[0] > 0.0){
	        		counter++;
	        	}
	        }
//	        System.out.println("After RANSAC: " + counter);
			return counter;
        }else{
        	return 0;
        }

		
	}


	public MatOfKeyPoint getKeypoints() {
		return keypoints;
	}



	public void setKeypoints(MatOfKeyPoint keypoints) {
		this.keypoints = keypoints;
	}



	public Mat getDescriptors() {
		return descriptors;
	}



	public void setDescriptors(Mat descriptors) {
		this.descriptors = descriptors;
	}
	
	

	
	
}
