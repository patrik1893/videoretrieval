package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.IOException;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;

public class BinaryEdgeHistogramFeature extends BinaryFixedLengthFeature {
	
	public static final String KEY = "BEdgeHistogram";
	
	private BinaryEdgeHistogramFeature(){
		
	}
	
	public BinaryEdgeHistogramFeature(double[] array) {
		super(array);
		// TODO Auto-generated constructor stub
	}

	public BinaryEdgeHistogramFeature(DoubleFV vector) {
		super(vector);
		// TODO Auto-generated constructor stub
	}

	static public BinaryEdgeHistogramFeature read (DataInput input) throws IOException{
		BinaryEdgeHistogramFeature feature = new BinaryEdgeHistogramFeature();
		feature.readFields(input);
		return feature;
	}




}
