/**
 * Copyright (c) 2011, The University of Southampton and the individual contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * 	Redistributions of source code must retain the above copyright notice,
 * 	this list of conditions and the following disclaimer.
 *
 *   *	Redistributions in binary form must reproduce the above copyright notice,
 * 	this list of conditions and the following disclaimer in the documentation
 * 	and/or other materials provided with the distribution.
 *
 *   *	Neither the name of the University of Southampton nor the names of its
 * 	contributors may be used to endorse or promote products derived from this
 * 	software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.feature_extraction;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openimaj.feature.DoubleFV;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.processing.resize.ResizeProcessor;
import org.openimaj.image.renderer.MBFImageRenderer;
import org.openimaj.math.geometry.shape.Rectangle;
import org.openimaj.video.processing.shotdetector.CombiShotDetector;
import org.openimaj.video.processing.shotdetector.HistogramVideoShotDetector;
import org.openimaj.video.processing.shotdetector.LocalHistogramVideoShotDetector;
import org.openimaj.video.processing.shotdetector.ShotBoundary;
import org.openimaj.video.processing.shotdetector.ShotDetectedListener;
import org.openimaj.video.processing.shotdetector.VideoKeyframe;
import org.openimaj.video.timecode.VideoTimecode;
import org.openimaj.video.xuggle.XuggleVideo;

import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import net.semanticmetadata.lire.imageanalysis.features.global.ColorLayout;
import net.semanticmetadata.lire.imageanalysis.features.global.EdgeHistogram;
import net.semanticmetadata.lire.imageanalysis.features.global.PHOG;
import net.semanticmetadata.lire.imageanalysis.features.global.ScalableColor;

import org.openimaj.video.Video;
import org.openimaj.video.VideoCache;
import org.openimaj.video.processing.pixels.MBFMeanVarianceField;
import org.openimaj.image.processing.face.detection.CLMDetectedFace;
import org.openimaj.image.processing.face.detection.CLMFaceDetector;



/**
 * Demonstration of the OpenIMAJ HistogramVideoShotDetector and visualisation thereof.
 * 
 * @author David Dupplaw (dpd@ecs.soton.ac.uk)
 * @created 1 Jun 2011
 */

public class WriteableFeatures {
	/**
	 * Testing code.
	 * 
	 * @param args
	 */
	public static void main(final String[] args) {
		
		extractHollywoodFaceFeatures();
		
		
			
	}
		
		
	    
			
			
			
		
		
	static void extractHollywoodActionFeatures(){
		
	int ges_number_videos = 810;
		
		
		FileReader fr;
		try {


			PrintWriter PHOG_PW = new PrintWriter (new FileOutputStream(
				    new File("/home/patrik/Hollywood2/Features/PHOG"), 
				    true /* append = true */));
			PrintWriter CL_PW = new PrintWriter (new FileOutputStream(
				    new File("/home/patrik/Hollywood2/Features/ColorLayout"), 
				    true /* append = true */));
			PrintWriter EH_PW = new PrintWriter (new FileOutputStream(
				    new File("/home/patrik/Hollywood2/Features/EdgeHistogram"), 
				    true /* append = true */));
			PrintWriter SC_PW = new PrintWriter (new FileOutputStream(
				    new File("/home/patrik/Hollywood2/Features/ScalableColor"), 
				    true /* append = true */));
			
			
			for (int videoNr = 726; videoNr <=  ges_number_videos; videoNr++){
				

				if (videoNr == 719){
					videoNr = 726;
				}
				
			    String formatted = String.format("%05d", videoNr);
			    
			    System.out.println(formatted);
				
				fr = new FileReader(
						"/home/patrik/Hollywood2/ShotBounds/actionclipautoautotrain" + formatted + ".sht" );
								
			    BufferedReader br = new BufferedReader(fr);		
				
				final XuggleVideo video = new XuggleVideo(
						"/home/patrik/Hollywood2/AVIClips/actionclipautoautotrain" + formatted + ".avi" );
								
				String line = br.readLine();
				String [] boundaries = line.split(" ");
				
				int bounds = boundaries.length;
				int nextBound = 0;
				
				int nextBoundFrame = 0;
				
				if (bounds > 0){
					nextBoundFrame = Integer.parseInt(boundaries[0]);
					if (nextBoundFrame == 0){
						 nextBound++;
						 if(nextBound < bounds){
							 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
						 }

					}
				}		
				
				int framecount = 0;
				int currentBound = 0;
								
				 for (final MBFImage mbfImage : video) {
					 
					 
										 
					 if (framecount == 0){

						 
						String fBound = String.format("%05d", currentBound);
							 
						GlobalFeature feature = new PHOG ();
						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
						
						PHOG_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
						
						feature = new ScalableColor ();
						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
						
						SC_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
						
						feature = new EdgeHistogram ();
						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
						
						EH_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
						
						feature = new ColorLayout ();
						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
						
						CL_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));

						
					 }
					 
					 if(nextBound < bounds){
						 if (framecount == nextBoundFrame){

							 currentBound = nextBoundFrame;

							String fBound = String.format("%05d", currentBound);

							GlobalFeature feature = new PHOG ();
							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
							
							PHOG_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
							
							feature = new ScalableColor ();
							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
							
							SC_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
							
							feature = new EdgeHistogram ();
							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
							
							EH_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
							
							feature = new ColorLayout ();
							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
							
							CL_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
							
						
							 nextBound++;
							 if(nextBound < bounds){
								 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
							 }
						 }
					 }
					 
					 framecount++;
					 
				 }
				 
							
				br.close();
				fr.close();
				video.close();

			}
			

			PHOG_PW.close();
			CL_PW.close();
			EH_PW.close();
			SC_PW.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		

	
	static void extractHollywoodFaceFeatures(){
		
		int ges_number_videos = 810;
			
			
			FileReader fr;
			try {


//				PrintWriter FACE_PW = new PrintWriter (
//						"/home/patrik/Hollywood2/Features/CLMFaces");
				

				PrintWriter FACE_PW = new PrintWriter (new FileOutputStream(
					    new File("/home/patrik/Hollywood2/Features/CLMFaces"), 
					    true /* append = true */));
				
				for (int videoNr = 726; videoNr <=  ges_number_videos; videoNr++){
				
				    
				    System.out.println(videoNr);
					
					if (videoNr == 719){
						videoNr = 726;
					}

				    String formatted = String.format("%05d", videoNr);
				    
					fr = new FileReader(
							"/home/patrik/Hollywood2/ShotBounds/actionclipautoautotrain" + formatted + ".sht" );
									
				    BufferedReader br = new BufferedReader(fr);		
					
					final XuggleVideo video = new XuggleVideo(
							"/home/patrik/Hollywood2/AVIClips/actionclipautoautotrain" + formatted + ".avi" );
									
					String line = br.readLine();
					String [] boundaries = line.split(" ");
					
					int bounds = boundaries.length;
					int nextBound = 0;
					
					int nextBoundFrame = 0;
					
					if (bounds > 0){
						nextBoundFrame = Integer.parseInt(boundaries[0]);
						if (nextBoundFrame == 0){
							 nextBound++;
							 if(nextBound < bounds){
								 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
							 }

						}
					}	
					
					int framecount = 0;
					int currentBound = 0;
									
					 for (final MBFImage mbfImage : video) {
						 
						 
											 
						 if (framecount == 0){

							String fBound = String.format("%05d", currentBound);
							 CLMFaceDetector detector = new CLMFaceDetector();
							List<CLMDetectedFace> facelist = detector.detectFaces(mbfImage.flatten());
							
							FACE_PW.print("actionclipautoautotrain" + formatted + "_" + fBound + "  " + facelist.size() + " ");
							
							for (CLMDetectedFace face : facelist) {
								FACE_PW.print(Arrays.toString(face.getPoseParameters().concatenate(face.getShapeParameters()).asDoubleVector()) + " ");
							}
							FACE_PW.print(System.lineSeparator());

							
						 }
						 
						 if(nextBound < bounds){
							 if (framecount == nextBoundFrame){

								 currentBound = nextBoundFrame;

								String fBound = String.format("%05d", currentBound);

								CLMFaceDetector detector = new CLMFaceDetector();
								List<CLMDetectedFace> facelist = detector.detectFaces(mbfImage.flatten());
								
								FACE_PW.print("actionclipautoautotrain" + formatted + "_" + fBound + "  " + facelist.size() + " ");
								
								for (CLMDetectedFace face : facelist) {
									FACE_PW.print(Arrays.toString(face.getPoseParameters().concatenate(face.getShapeParameters()).asDoubleVector()) + " ");
								}
								FACE_PW.print(System.lineSeparator());
							
								 nextBound++;
								 if(nextBound < bounds){
									 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
								 }
							 }
						 }
						 
						 framecount++;
						 
					 }
					 
								
					br.close();
					fr.close();
					video.close();

				}
				

				FACE_PW.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	

	
}
