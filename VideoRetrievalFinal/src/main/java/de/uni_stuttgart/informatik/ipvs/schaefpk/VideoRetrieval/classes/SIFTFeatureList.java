package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class SIFTFeatureList implements FeatureList {

	private SIFTListFeature siftlist;
	private SIFTMeanFeature siftmean;

	public SIFTFeatureList() {
		super();
		this.siftlist = new SIFTListFeature();
		this.siftmean = new SIFTMeanFeature();
	}
	
	public SIFTFeatureList(SIFTListFeature siftlist, SIFTMeanFeature siftmean) {
		super();
		this.siftlist = siftlist;
		this.siftmean = siftmean;
	}

	@Override
	public double compareToInsert(FeatureList other) {
		return siftlist.compareTo(((SIFTFeatureList)other).getSiftlist());
	}

	@Override
	public double compareToQuery(FeatureList other) {

		return siftmean.compareTo(((SIFTFeatureList)other).getSiftmean());
	}

	public SIFTListFeature getSiftlist() {
		return siftlist;
	}


	public SIFTMeanFeature getSiftmean() {
		return siftmean;
	}


	static public SIFTFeatureList read(DataInput in){
		SIFTFeatureList fl = new SIFTFeatureList();
		try {
			fl.readFields(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fl;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		siftlist.readFields(in);
		siftmean.readFields(in);
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		siftlist.write(arg0);
		siftmean.write(arg0);
	}
	
	

}
