package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.feature_extraction;

import java.awt.Toolkit;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.feature.dense.gradient.PHOG;
import org.openimaj.image.feature.local.engine.asift.ColourASIFTEngine;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.image.processing.resize.ResizeProcessor;
import org.openimaj.image.renderer.MBFImageRenderer;
import org.openimaj.math.geometry.shape.Rectangle;
import org.openimaj.video.processing.shotdetector.CombiShotDetector;
import org.openimaj.video.processing.shotdetector.HistogramVideoShotDetector;
import org.openimaj.video.processing.shotdetector.LocalHistogramVideoShotDetector;
import org.openimaj.video.processing.shotdetector.ShotBoundary;
import org.openimaj.video.processing.shotdetector.ShotDetectedListener;
import org.openimaj.video.processing.shotdetector.VideoKeyframe;
import org.openimaj.video.timecode.VideoTimecode;
import org.openimaj.video.xuggle.XuggleVideo;

import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import net.semanticmetadata.lire.imageanalysis.features.global.ColorLayout;
import net.semanticmetadata.lire.imageanalysis.features.global.EdgeHistogram;
import net.semanticmetadata.lire.imageanalysis.features.global.ScalableColor;

import org.openimaj.video.Video;
import org.openimaj.video.VideoCache;
import org.openimaj.video.processing.pixels.MBFMeanVarianceField;
import org.openimaj.image.processing.face.detection.CLMDetectedFace;
import org.openimaj.image.processing.face.detection.CLMFaceDetector;


public class ASIFTExtractionHollywood {
	/**
	 * Testing code.
	 * 
	 * @param args
	 */
	public static void main(final String[] args) {
		
		extractASIFT();
		
		
			
	}
		

		
	
	static void extractASIFT(){
		
		int ges_number_videos = 50;
		
		
		FileReader fr;
		try {

			
			for (int videoNr = 4; videoNr <=  ges_number_videos; videoNr++){
				
				System.out.println(" --- " + videoNr + " --- ");
				
				if (videoNr == 719){
					videoNr = 726;
				}
				
			    String formatted = String.format("%05d", videoNr);
				
				fr = new FileReader(
						"/home/patrik/Hollywood2/ShotBounds/actionclipautoautotrain" + formatted + ".sht" );
				
				
			    BufferedReader br = new BufferedReader(fr);		
				
				final XuggleVideo video = new XuggleVideo(
						"/home/patrik/Hollywood2/AVIClips/actionclipautoautotrain" + formatted + ".avi" );
								
				String line = br.readLine();
				String [] boundaries = line.split(" ");
				
				int bounds = boundaries.length;
				int nextBound = 0;
				
				int nextBoundFrame = 0;
				
				if (bounds > 0){
					nextBoundFrame = Integer.parseInt(boundaries[0]);
					if (nextBoundFrame == 0){
						 nextBound++;
						 if(nextBound < bounds){
							 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
						 }

					}
				}
												
				int framecount = 0;
				int currentBound = 0;
								
				 for (final MBFImage mbfImage : video) {
					 		 
					 					 
					 if (framecount == 0){

						 
						String fBound = String.format("%05d", currentBound);
							 
						File file = new File (
								"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/ASIFT" + fBound);

//						PrintWriter pw = new PrintWriter(file);
						DataOutputStream pw = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
						final ColourASIFTEngine engine = new ColourASIFTEngine();
						LocalFeatureList<Keypoint> features = engine.findKeypoints(mbfImage);
						System.out.println(features.size());
						features.writeBinary(pw);
//						features.writeASCII(pw);
						pw.close();
						
						
					 }
					 
					 if(nextBound < bounds){
						 if (framecount == nextBoundFrame){
							 

							String fBound = String.format("%05d", nextBoundFrame);
							
								
							File file = new File (
									"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/ASIFT" + fBound);

//							PrintWriter pw = new PrintWriter(file);
							DataOutputStream pw = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));

							final ColourASIFTEngine engine = new ColourASIFTEngine();
							LocalFeatureList<Keypoint> features = engine.findKeypoints(mbfImage);
							System.out.println(features.size());
							features.writeBinary(pw);
//							features.writeASCII(pw);
							pw.close();
							
							 currentBound = nextBoundFrame;
							 nextBound++;
							 if(nextBound < bounds){
								 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
							 }
						 }
					 }
					 
					 framecount++;
					 
				 }
				 
				br.close();
				fr.close();
				video.close();
				}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	

	
}
