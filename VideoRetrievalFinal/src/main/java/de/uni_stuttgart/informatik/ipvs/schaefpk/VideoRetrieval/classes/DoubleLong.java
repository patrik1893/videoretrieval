package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

public class DoubleLong implements Comparable<DoubleLong> {

	double dist;
	long vertex_id;
	String name;
	
	public DoubleLong (double d, long l, String name){
		dist = d;
		vertex_id = l;
		this.name = name;
	}
	
	
	
	public double getDist() {
		return dist;
	}



	public void setDist(double dist) {
		this.dist = dist;
	}



	public long getVertex_id() {
		return vertex_id;
	}



	public void setVertex_id(long vertex_id) {
		this.vertex_id = vertex_id;
	}

	


	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	@Override
	public int compareTo(DoubleLong o) {
		// TODO Auto-generated method stub
		return Double.compare(o.getDist(), dist);
	}

}
