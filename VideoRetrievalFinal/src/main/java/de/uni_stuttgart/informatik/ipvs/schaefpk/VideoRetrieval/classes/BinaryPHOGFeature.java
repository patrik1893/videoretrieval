package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.IOException;

import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;

public class BinaryPHOGFeature extends BinaryFixedLengthFeature {
	
	public static final String KEY = "BPHOG";
	
	private BinaryPHOGFeature(){
		
	}
	
	
	
	public BinaryPHOGFeature(double[] array) {
		super(array);
		// TODO Auto-generated constructor stub
	}



	public BinaryPHOGFeature(DoubleFV vector) {
		super(vector);
		// TODO Auto-generated constructor stub
	}



	static public BinaryPHOGFeature read (DataInput input) throws IOException{
		BinaryPHOGFeature feature = new BinaryPHOGFeature();
		feature.readFields(input);
		return feature;
	}




}
