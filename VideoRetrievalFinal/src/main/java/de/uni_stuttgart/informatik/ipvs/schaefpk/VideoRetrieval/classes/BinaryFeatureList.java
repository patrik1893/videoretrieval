package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.hadoop.io.Writable;

public class BinaryFeatureList extends StandardFeatureList implements Writable {
	
	private BinaryFeatureList(){
		
	}
	
	public BinaryFeatureList(ArrayList<Entry<String, Feature>> features) {
		super(features);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void readFields(DataInput input) throws IOException {
		int numberFeatures = input.readInt();
		features = new ArrayList<Entry<String, Feature>> ();
		for (int i = 0; i < numberFeatures; i++){
			String featureClass = input.readUTF();
			Feature feature = BinaryFeatureFactory.getFeature(featureClass, input);
			features.add(new AbstractMap.SimpleEntry<String,Feature>(featureClass, feature));
		}
	}

	@Override
	public void write(DataOutput output) throws IOException {
		output.writeInt(features.size());
		for (int i = 0; i < features.size(); i++){
			output.writeUTF(features.get(i).getKey());
			features.get(i).getValue().write(output);
		}
		
	}
	
    public static BinaryFeatureList read(DataInput in) throws IOException {
    	BinaryFeatureList fl = new BinaryFeatureList();
    	fl.readFields(in);
    	return fl;
    
    
    }


}