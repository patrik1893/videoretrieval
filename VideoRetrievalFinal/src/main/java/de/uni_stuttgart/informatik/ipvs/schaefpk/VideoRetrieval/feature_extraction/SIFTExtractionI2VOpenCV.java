package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.feature_extraction;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.KeyPoint;
import org.opencv.highgui.Highgui;
import org.openimaj.feature.local.list.FileLocalFeatureList;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.list.MemoryLocalFeatureList;
import org.openimaj.feature.local.matcher.BasicTwoWayMatcher;
import org.openimaj.feature.local.matcher.FastBasicKeypointMatcher;
import org.openimaj.feature.local.matcher.FastEuclideanKeypointMatcher;
import org.openimaj.feature.local.matcher.LocalFeatureMatcher;
import org.openimaj.feature.local.matcher.MatchingUtilities;
import org.openimaj.feature.local.matcher.VotingKeypointMatcher;
import org.openimaj.feature.local.matcher.consistent.ConsistentLocalFeatureMatcher2d;
import org.openimaj.feature.local.matcher.consistent.LocalConsistentKeypointMatcher;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.feature.local.engine.DoGColourSIFTEngine;
import org.openimaj.image.feature.local.engine.DoGSIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ASIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ColourASIFTEngine;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.transforms.HomographyModel;
import org.openimaj.math.geometry.transforms.HomographyRefinement;
import org.openimaj.math.geometry.transforms.estimation.RobustHomographyEstimator;
import org.openimaj.math.geometry.transforms.residuals.SingleImageTransferResidual2d;
import org.openimaj.math.model.fit.RANSAC;
import org.openimaj.util.pair.Pair;

import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.OpenCVUtils;
import nu.pattern.OpenCV;


public class SIFTExtractionI2VOpenCV {

	public static void main(String[] args) throws IOException {
		// Read the images from two streams
		ArrayList<File> files = KeyframeProvider.getFileList("/home/patrik/database/keyframes");

		OpenCV.loadShared() ;   	
		
		DataOutputStream pw ;


		for (int i = 0; i < files.size(); i++){
			

			String str = files.get(i).getCanonicalPath();
	        str = str.substring(32, str.length()-4).replace('/', '_') + ".sift";
	        if(str.startsWith("201302")){
	        	
	        	 Mat image = Highgui.imread(files.get(i).getCanonicalPath(), Highgui.CV_LOAD_IMAGE_COLOR);

	             MatOfKeyPoint objectKeyPoints = new MatOfKeyPoint();
	             FeatureDetector featureDetector = FeatureDetector.create(FeatureDetector.SIFT);
	             featureDetector.detect(image, objectKeyPoints);
	             List<KeyPoint> keypoints = objectKeyPoints.toList();
	             Point[] points = new Point[keypoints.size()];
	             for (int j = 0; j < keypoints.size(); j ++){
	            	 points[j] = keypoints.get(j).pt;
	             }
	             
	             MatOfPoint2f pointMat =  new MatOfPoint2f(points);

	             System.out.println(pointMat.type());
	             System.out.println("Number keypoints:" + keypoints.size());

	             if (objectKeyPoints.toList().size() > 0){
		             MatOfKeyPoint objectDescriptors = new MatOfKeyPoint();
		             DescriptorExtractor descriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.SIFT);
		             descriptorExtractor.compute(image, objectKeyPoints, objectDescriptors);

		             System.out.println(objectDescriptors.type());
		             System.out.println(objectDescriptors.rows());
		             System.out.println(objectDescriptors.cols());
		             
					pw = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("/home/patrik/database/siftOpenCV/" + str)));
				
					pw.writeUTF(OpenCVUtils.matToJson(pointMat));
					pw.writeUTF(OpenCVUtils.matToJson(objectDescriptors));
					pw.close();
	             }
	        }

		}
	}
}