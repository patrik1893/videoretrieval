package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;

import org.apache.hadoop.io.Writable;

public abstract class Feature implements Writable {
	
	
	@Override
	abstract public void readFields(DataInput arg0) throws IOException;

	@Override
	abstract public void write(DataOutput arg0) throws IOException ;
	
	abstract public double compareTo(Feature otherFeature) ;

}
