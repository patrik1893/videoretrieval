package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import org.apache.hadoop.io.Writable;

public interface FeatureList extends Writable{

	public double compareToInsert (FeatureList other);
	
	public double compareToQuery(FeatureList other);
	
}
