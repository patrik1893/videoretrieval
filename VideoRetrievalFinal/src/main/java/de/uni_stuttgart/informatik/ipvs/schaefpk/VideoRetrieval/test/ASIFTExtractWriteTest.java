/**
 * Copyright (c) 2011, The University of Southampton and the individual contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * 	Redistributions of source code must retain the above copyright notice,
 * 	this list of conditions and the following disclaimer.
 *
 *   *	Redistributions in binary form must reproduce the above copyright notice,
 * 	this list of conditions and the following disclaimer in the documentation
 * 	and/or other materials provided with the distribution.
 *
 *   *	Neither the name of the University of Southampton nor the names of its
 * 	contributors may be used to endorse or promote products derived from this
 * 	software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.test;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.openimaj.feature.local.list.FileLocalFeatureList;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.list.MemoryLocalFeatureList;
import org.openimaj.feature.local.matcher.BasicTwoWayMatcher;
import org.openimaj.feature.local.matcher.FastBasicKeypointMatcher;
import org.openimaj.feature.local.matcher.FastEuclideanKeypointMatcher;
import org.openimaj.feature.local.matcher.LocalFeatureMatcher;
import org.openimaj.feature.local.matcher.MatchingUtilities;
import org.openimaj.feature.local.matcher.VotingKeypointMatcher;
import org.openimaj.feature.local.matcher.consistent.ConsistentLocalFeatureMatcher2d;
import org.openimaj.feature.local.matcher.consistent.LocalConsistentKeypointMatcher;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.feature.local.engine.DoGColourSIFTEngine;
import org.openimaj.image.feature.local.engine.DoGSIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ASIFTEngine;
import org.openimaj.image.feature.local.engine.asift.ColourASIFTEngine;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.transforms.HomographyModel;
import org.openimaj.math.geometry.transforms.HomographyRefinement;
import org.openimaj.math.geometry.transforms.estimation.RobustHomographyEstimator;
import org.openimaj.math.geometry.transforms.residuals.SingleImageTransferResidual2d;
import org.openimaj.math.model.fit.RANSAC;
import org.openimaj.util.pair.Pair;

/**
 * Example showing how to extract ASIFT features and match them.
 * 
 * @author Jonathon Hare (jsh2@ecs.soton.ac.uk)
 * @author Sina Samangooei (ss@ecs.soton.ac.uk)
 * 
 */
public class ASIFTExtractWriteTest {
	/**
	 * Main method
	 * 
	 * @param args
	 *            ignored
	 * @throws IOException
	 *             if the image can't be read
	 */
	public static void main(String[] args) throws IOException {
		// Read the images from two streams
		File [] fileArray = new File [8];
		FImage [] images = new FImage[8];
		for (int i = 1; i <= 8; i++){
			fileArray [i-1] = new File("resources/00000" + i + ".jpg");
			images [i-1] = ImageUtilities.readF(fileArray [i-1]);
		}
//		
//		ArrayList <PrintWriter> pw = new ArrayList <PrintWriter>();
//		pw.add(new PrintWriter("resources/oooout1"));
//		pw.add(new PrintWriter("resources/oooout2"));
//		pw.add(new PrintWriter("resources/oooout3"));
//		pw.add(new PrintWriter("resources/oooout4"));
//		pw.add(new PrintWriter("resources/oooout5"));
//		pw.add(new PrintWriter("resources/oooout6"));
//		pw.add(new PrintWriter("resources/oooout7"));
//		pw.add(new PrintWriter("resources/oooout8"));
		

		ArrayList <DataOutputStream> pw = new ArrayList <DataOutputStream>();
		pw.add(new DataOutputStream(new FileOutputStream("resources/o1")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o2")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o3")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o4")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o5")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o6")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o7")));
		pw.add(new DataOutputStream(new FileOutputStream("resources/o8")));

		// Prepare the engine to the parameters in the IPOL demo
//		final ColourASIFTEngine engine = new ColourASIFTEngine();

		DoGSIFTEngine engine = new DoGSIFTEngine();
//		engine.getOptions().setDoubleInitialImage(true);
		
		ArrayList <LocalFeatureList<Keypoint>> keypoints = new ArrayList <LocalFeatureList<Keypoint>>();
////		
		for (int i = 1; i <= 8; i++){
			keypoints.add(engine.findFeatures(images[i-1]));
			keypoints.get(i-1).writeBinary(pw.get(i-1));
			pw.get(i-1).close();
			System.out.println("Extracted keypoints " + keypoints.get(i-1).size());
		}
	}
}