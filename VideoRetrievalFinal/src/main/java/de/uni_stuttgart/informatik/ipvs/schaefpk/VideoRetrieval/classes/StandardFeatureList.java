package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.hadoop.io.Writable;

public abstract class StandardFeatureList implements Writable, FeatureList {
	
	ArrayList<Entry<String, Feature>> features;
	
	protected StandardFeatureList (){
	}
	
	public StandardFeatureList (ArrayList<Entry<String, Feature>> features){
		this.features = features;
	}

	@Override
	abstract public void readFields(DataInput arg0) throws IOException;

	@Override
	abstract public void write(DataOutput arg0) throws IOException;
	
	public double compareToInsert (FeatureList otherFL){
		double sum = 0.0;
		for(int i = 0; i < features.size(); i++){			
			sum += features.get(i).getValue().compareTo(((StandardFeatureList)otherFL).getFeatures().get(i).getValue());			
		}
		sum /= features.size();
		
		return sum;
	}
	
	public double compareToQuery (FeatureList otherFL){
		double sum = 0.0;
		for(int i = 0; i < features.size(); i++){			
			sum += features.get(i).getValue().compareTo(((StandardFeatureList)otherFL).getFeatures().get(i).getValue());			
		}
		sum /= features.size();
		
		return sum;
	}
	
	public ArrayList<Entry<String, Feature>>  getFeatures(){
		return features;
	}

	public void setFeatures(ArrayList<Entry<String, Feature>> features) {
		this.features = features;
	}

	
}
