package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes;

public class DoubleText implements Comparable<DoubleText> {

	double dist;
	String name;
	
	public DoubleText (double d, String name){
		dist = d;
		this.name = name;
	}
	
	
	
	public double getDist() {
		return dist;
	}



	public void setDist(double dist) {
		this.dist = dist;
	}


	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	@Override
	public int compareTo(DoubleText o) {
		// TODO Auto-generated method stub
		return Double.compare(dist, o.getDist());
	}

}
