
package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.feature_extraction;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openimaj.feature.DoubleFV;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.feature.dense.gradient.PHOG;
import org.openimaj.video.xuggle.XuggleVideo;
import org.openimaj.image.processing.face.detection.CLMDetectedFace;
import org.openimaj.image.processing.face.detection.CLMFaceDetector;




public class VideoShotDetectorVisualisation {
	/**
	 * Testing code.
	 * 
	 * @param args
	 */
	public static void main(final String[] args) {
		
		extractHollywoodActionFeatures();
		
		
			
	}
		
		
	    
			
			
			
		
		
	static void extractHollywoodActionFeatures(){
		
	int ges_number_videos = 810;
		
		
		FileReader fr;
		try {


			PrintWriter PHOG_PW = new PrintWriter (new FileOutputStream(
				    new File("/home/patrik/Hollywood2/Features/test"), 
				    true /* append = true */));
//			PrintWriter CL_PW = new PrintWriter (new FileOutputStream(
//				    new File("/home/patrik/Hollywood2/Features/ColorLayout"), 
//				    true /* append = true */));
//			PrintWriter EH_PW = new PrintWriter (new FileOutputStream(
//				    new File("/home/patrik/Hollywood2/Features/EdgeHistogram"), 
//				    true /* append = true */));
//			PrintWriter SC_PW = new PrintWriter (new FileOutputStream(
//				    new File("/home/patrik/Hollywood2/Features/ScalableColor"), 
//				    true /* append = true */));
			
			
			for (int videoNr = 1; videoNr <=  ges_number_videos; videoNr++){
				
				if (videoNr == 719){
					videoNr = 726;
				}
				
			    String formatted = String.format("%05d", videoNr);
			    
			    System.out.println(formatted);
				
				fr = new FileReader(
						"/home/patrik/Hollywood2/ShotBounds/actionclipautoautotrain" + formatted + ".sht" );
								
			    BufferedReader br = new BufferedReader(fr);		
				
				final XuggleVideo video = new XuggleVideo(
						"/home/patrik/Hollywood2/AVIClips/actionclipautoautotrain" + formatted + ".avi" );
								
				String line = br.readLine();
				String [] boundaries = line.split(" ");
				
				int bounds = boundaries.length;
				int nextBound = 0;
				
				int nextBoundFrame = 0;
				
				if (bounds > 0){
					nextBoundFrame = Integer.parseInt(boundaries[0]);
					if (nextBoundFrame == 0){
						 nextBound++;
						 if(nextBound < bounds){
							 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
						 }

					}
				}		
				
				int framecount = 0;
				int currentBound = 0;
								
				 for (final MBFImage mbfImage : video) {
					 
					 
										 
					 if (framecount == 0){

						 
						String fBound = String.format("%05d", currentBound);
							 

						PHOG phog = new PHOG ();
						phog.analyseImage(mbfImage.flatten());
						
						PHOG_PW.print("actionclipautoautotrain" + formatted + "_" + fBound + "  ");
						phog.getFeatureVector().writeASCII(PHOG_PW);
												
//						feature = new ScalableColor ();
//						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
//						
//						SC_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
//						
//						feature = new EdgeHistogram ();
//						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
//						
//						EH_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
//						
//						feature = new ColorLayout ();
//						feature.extract(ImageUtilities.createBufferedImage(mbfImage));
//						
//						CL_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));

						
					 }
					 
					 if(nextBound < bounds){
						 if (framecount == nextBoundFrame){

							 currentBound = nextBoundFrame;

							String fBound = String.format("%05d", currentBound);

							PHOG phog = new PHOG ();
							phog.analyseImage(mbfImage.flatten());
							
							PHOG_PW.print("actionclipautoautotrain" + formatted + "_" + fBound + "  ");
							phog.getFeatureVector().writeASCII(PHOG_PW);
							
//							feature = new ScalableColor ();
//							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
							
//							SC_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
							
//							feature = new EdgeHistogram ();
//							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
//							
//							EH_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
//							
//							feature = new ColorLayout ();
//							feature.extract(ImageUtilities.createBufferedImage(mbfImage));
//							
//							CL_PW.println("actionclipautoautotrain" + formatted + "_" + fBound + "  "+ Arrays.toString(feature.getFeatureVector()));
//							
						
							 nextBound++;
							 if(nextBound < bounds){
								 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
							 }
						 }
					 }
					 
					 framecount++;
					 
				 }
				 
							
				br.close();
				fr.close();
				video.close();

			}
			

			PHOG_PW.close();
//			CL_PW.close();
//			EH_PW.close();
//			SC_PW.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	

		
	static void extractHollywoodActionKeyframes(){
		
		int ges_number_videos = 810;
		
		
		FileReader fr;
		try {

			
			for (int videoNr = 1; videoNr <=  ges_number_videos; videoNr++){
				
				System.out.println(videoNr);
				
				if (videoNr == 719){
					videoNr = 726;
				}
				
			    String formatted = String.format("%05d", videoNr);
				
				fr = new FileReader(
						"/home/patrik/Hollywood2/ShotBounds/actionclipautoautotrain" + formatted + ".sht" );
				
//				System.out.println(formatted);
				
			    BufferedReader br = new BufferedReader(fr);		
				
				final XuggleVideo video = new XuggleVideo(
						"/home/patrik/Hollywood2/AVIClips/actionclipautoautotrain" + formatted + ".avi" );
				
//				PrintWriter pw2 = new PrintWriter (
//						"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/" + FName + ".ftr" );
				
				String line = br.readLine();
				String [] boundaries = line.split(" ");
				
				int bounds = boundaries.length;
				int nextBound = 0;
				
				int nextBoundFrame = 0;
				
				if (bounds > 0){
					nextBoundFrame = Integer.parseInt(boundaries[0]);
					if (nextBoundFrame == 0){
						 nextBound++;
						 if(nextBound < bounds){
							 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
						 }

					}
				}
				
				

				MyOwnMBFMeanVarianceField vf = new MyOwnMBFMeanVarianceField();
								
				int framecount = 0;
				int currentBound = 0;
								
				 for (final MBFImage mbfImage : video) {
					 
					 
					 
//					 System.out.println(framecount + " " + currentBound);
					 
					 if (framecount == 0){

						 
						String fBound = String.format("%05d", currentBound);
							 
						File keyframe = new File (
							"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/keyframe" + fBound + ".jpg");
						keyframe.mkdirs();

						ImageUtilities.write(mbfImage ,keyframe);
						
						
					 }
					 
					 if(nextBound < bounds){
						 if (framecount == nextBoundFrame){
							 

							String fBound = String.format("%05d", currentBound);
							String fBound2 = String.format("%05d", nextBoundFrame);
							
							 File keyframe = new File (
										"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/keyframe" + fBound2 + ".jpg");
								keyframe.mkdirs();
										
							
								
							File meanframe = new File (
										"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/meanframe" + fBound + ".jpg");
										

							File varframe = new File (
										"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/varframe" + fBound + ".jpg");
														
							
							 ImageUtilities.write(mbfImage ,keyframe);
							 MBFImage meanimage = vf.getMean(); 
							 
							 ImageUtilities.write(meanimage ,meanframe);
							 ImageUtilities.write(vf.getVariance() ,varframe);
							 
							 vf.reset();
							 currentBound = nextBoundFrame;
							 nextBound++;
							 if(nextBound < bounds){
								 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
							 }
						 }
					 }
					 
					 vf.analyseFrame(mbfImage);
					 framecount++;
					 
				 }
				 
				String fBound = String.format("%05d", currentBound);
							

				File meanframe = new File (
							"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/meanframe" + fBound + ".jpg");
							

				File varframe = new File (
							"/home/patrik/Hollywood2/Features/actionclipautoautotrain" + formatted + "/varframe" + fBound + ".jpg");

				 MBFImage meanimage = vf.getMean();
				 ImageUtilities.write(meanimage ,meanframe);
				ImageUtilities.write(vf.getVariance() ,varframe);					 			
				
				vf.close();
				br.close();
				fr.close();
				video.close();
				}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	static void extractHollywoodFaceFeatures(){
		
		int ges_number_videos = 810;
			
			
			FileReader fr;
			try {


//				PrintWriter FACE_PW = new PrintWriter (
//						"/home/patrik/Hollywood2/Features/CLMFaces");
				

				PrintWriter FACE_PW = new PrintWriter (new FileOutputStream(
					    new File("/home/patrik/Hollywood2/Features/CLMFaces"), 
					    true /* append = true */));
				
				for (int videoNr = 726; videoNr <=  ges_number_videos; videoNr++){
				
				    
				    System.out.println(videoNr);
					
					if (videoNr == 719){
						videoNr = 726;
					}

				    String formatted = String.format("%05d", videoNr);
				    
					fr = new FileReader(
							"/home/patrik/Hollywood2/ShotBounds/actionclipautoautotrain" + formatted + ".sht" );
									
				    BufferedReader br = new BufferedReader(fr);		
					
					final XuggleVideo video = new XuggleVideo(
							"/home/patrik/Hollywood2/AVIClips/actionclipautoautotrain" + formatted + ".avi" );
									
					String line = br.readLine();
					String [] boundaries = line.split(" ");
					
					int bounds = boundaries.length;
					int nextBound = 0;
					
					int nextBoundFrame = 0;
					
					if (bounds > 0){
						nextBoundFrame = Integer.parseInt(boundaries[0]);
						if (nextBoundFrame == 0){
							 nextBound++;
							 if(nextBound < bounds){
								 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
							 }

						}
					}	
					
					int framecount = 0;
					int currentBound = 0;
									
					 for (final MBFImage mbfImage : video) {
						 
						 
											 
						 if (framecount == 0){

							String fBound = String.format("%05d", currentBound);
							 CLMFaceDetector detector = new CLMFaceDetector();
							List<CLMDetectedFace> facelist = detector.detectFaces(mbfImage.flatten());
							
							FACE_PW.print("actionclipautoautotrain" + formatted + "_" + fBound + "  " + facelist.size() + " ");
							
							for (CLMDetectedFace face : facelist) {
								FACE_PW.print(Arrays.toString(face.getPoseParameters().concatenate(face.getShapeParameters()).asDoubleVector()) + " ");
							}
							FACE_PW.print(System.lineSeparator());

							
						 }
						 
						 if(nextBound < bounds){
							 if (framecount == nextBoundFrame){

								 currentBound = nextBoundFrame;

								String fBound = String.format("%05d", currentBound);

								CLMFaceDetector detector = new CLMFaceDetector();
								List<CLMDetectedFace> facelist = detector.detectFaces(mbfImage.flatten());
								
								FACE_PW.print("actionclipautoautotrain" + formatted + "_" + fBound + "  " + facelist.size() + " ");
								
								for (CLMDetectedFace face : facelist) {
									FACE_PW.print(Arrays.toString(face.getPoseParameters().concatenate(face.getShapeParameters()).asDoubleVector()) + " ");
								}
								FACE_PW.print(System.lineSeparator());
							
								 nextBound++;
								 if(nextBound < bounds){
									 nextBoundFrame = Integer.parseInt(boundaries[nextBound]);
								 }
							 }
						 }
						 
						 framecount++;
						 
					 }
					 
								
					br.close();
					fr.close();
					video.close();

				}
				

				FACE_PW.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	

	
}
