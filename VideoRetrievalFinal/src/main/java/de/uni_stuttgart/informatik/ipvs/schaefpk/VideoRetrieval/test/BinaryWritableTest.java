package de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.test;

import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.openimaj.feature.DoubleFV;
import org.openimaj.image.ImageUtilities;


import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTDataObject;
import de.uni_stuttgart.informatik.ipvs.schaefpk.VideoRetrieval.classes.SIFTFeatureList;
import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import net.semanticmetadata.lire.imageanalysis.features.global.ColorLayout;
import net.semanticmetadata.lire.imageanalysis.features.global.EdgeHistogram;
import net.semanticmetadata.lire.imageanalysis.features.global.PHOG;
import net.semanticmetadata.lire.imageanalysis.features.global.ScalableColor;

public class BinaryWritableTest {
	
	
	
	public static void main(String[] args){
		
		File file = new File("resources/iotest.out");
		DataInputStream in;
		
		SIFTDataObject obj;
		try {
			in = new DataInputStream(new FileInputStream(file));
			obj = SIFTDataObject.read(in);
			System.out.println(obj.getName());
			System.out.println(((SIFTFeatureList)obj.getFeatureList()).getSiftlist().getKeypoints().size());

			in.close();
			DataOutputStream out = new DataOutputStream(new FileOutputStream("resources/ficken"));
			obj.write(out);
			out.close();
			in = new DataInputStream(new FileInputStream("resources/ficken"));
			obj = SIFTDataObject.read(in);
			System.out.println(obj.getName());
			System.out.println(((SIFTFeatureList)obj.getFeatureList()).getSiftlist().getKeypoints().size());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
