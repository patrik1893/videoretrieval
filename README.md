There are 4 different packages:

Classes:
Abstract and explicit classes of features and other needed standard classes

Feature Extraction:
Different methods to gain features of a video datasets / keyframes

Giraph:
BlockFactory and pieces for VideoRetrieval. GiraphStarter configures and starts the Giraph job.

Test:
Several routines for test purposes